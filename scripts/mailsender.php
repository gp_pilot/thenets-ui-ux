<?php

date_default_timezone_set('Etc/UTC');
require 'external-library/PHPMailer-master/PHPMailerAutoload.php';

/**
 * Description of mailsender
 * @author Sławomir Zych
 */
class mailsender {
    
    public $mail;
   
    public function __construct() {
        
        $this->mail = new PHPMailer;
        
        $this->mail->isSMTP();
        $this->mail->CharSet = 'UTF-8';
        
        $this->mail->SMTPDebug = 0;
        //Ask for HTML-friendly debug output
        $this->mail->Debugoutput = 'html';
        //Set the hostname of the mail server
        $this->mail->Host = "mail.thenets.pl";
        //Set the SMTP port number - likely to be 25, 465 or 587
        $this->mail->Port = 587;
        //Whether to use SMTP authentication
        $this->mail->SMTPAuth = true;
        //Username to use for SMTP authentication
        $this->mail->Username = "hello@thenets.pl";
        //Password to use for SMTP authentication
        $this->mail->Password = "mp1@&6D%#Pyv";
        //Set who the message is to be sent from
        $this->mail->setFrom('hello@thenets.pl', 'The Nets');
        //Set an alternative reply-to address
        $this->mail->addReplyTo('hello@thenets.pl', 'The Nets');
       
    }    
    
    public function sendToClient($email,$surnameName){
        
        $this->mail->clearAddresses();
        $this->mail->addBCC('slawomir.zych@thenets.pl');
        $this->mail->addAddress($email);
        
        //Set the subject line
        $this->mail->Subject = 'Gratulacje! Właśnie zrobiłeś ważny krok w kierunku funkjonalnego UI/UX!';
        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body

        
      $msg ='<html>
        <head>           
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </head>
        <body>
            <table>
                <tr>
                    <td><img width="100px" src="cid:logo" /></td>
                    <td valign="middle" align="center" style="padding:0 0 0 30px">Cześć <b>'.$surnameName.'! </b></td>
                </tr>
            </table>
           
            <table>
                <tr>
                    <td style="background:#F7F7F7; padding:20px;">
Widzimy, że interesuje Cię digitalizacja biznesu i funkcjonalne projektowanie w zgodzie z UI/UX. Trafiłeś w odpowiednie miejsce. <br>Nasze działania pomagają zwiększyć konwersję stron internetowych i aplikacji.
<br>
Skutecznie doradzamy naszym partnerom biznesowym, jak dzięki funkcjonalnym witrynom, zaprojektowanym według podejścia User Centered Design, zwiększyć sprzedaż w Internecie.
Twoje zapytanie przekażemy niezwłocznie do naszego działu UI/UX. Czekaj na e-mail, odpiszemy najszybciej jak to możliwe.
<br><br>
pozdrawiamy The Nets<br>

<br><br>
<span style="font-size:12px">PS Od dnia 25 maja 2018 r. weszło w życie Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27.04.2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych (RODO). Ponieważ ochrona danych osobowych jest od zawsze jedną z najważniejszych wartości The Nets, czujemy się w obowiązku poinformować Cię o zasadach przetwarzania Twoich danych osobowych.
Administratorem Twoich danych osobowych i gwarantem ich bezpieczeństwa jest The Nets Sp. z o.o. z siedzibą przy ulicy Kazimierza Morawskiego 5, 30-102 Kraków. Twoje dane osobowe będą przez nas przetwarzane przez okres niezbędny do realizacji działań marketingowych. Zgodę na ich przetwarzanie możesz w każdej chwili wycofać. Przysługuje Ci ponadto prawo dostępu do Twoich danych oraz do ich poprawiania, prawo wniesienia sprzeciwu wobec przetwarzania danych, prawo żądania usunięcia swoich danych osobowych, prawo ograniczenia przetwarzania danych, prawo uzyskania kopii swoich danych oraz ich przeniesienia. Możesz to zrobić kontaktując się z nami przez hello@thenets.pl.
<br><br>
   </td>
                </tr>
            </table>
        </body>
    </html>';
        
        
        preg_match_all('/src="cid:(.+?)"/', $msg, $imgs);

        foreach($imgs[1] as $img){
            $this->mail->addEmbeddedImage(
                    'img/'.$img.'.jpg',
                    $img,
                    $img.'.jpg',
                    'base64',
                    'image/jpeg'
                );
        }

        $this->mail->msgHTML($msg);

        //send the message, check for errors
        if (!$this->mail->send()) {
            echo "Mailer Error: " . $this->mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }
    
    public function sendToUser($email,$surnameName,$www,$company,$description,$phone,$agree2,$agree3,$agree4){
         
        $this->mail->clearAddresses();
        $this->mail->addAddress('hello@thenets.pl');
        $this->mail->addBCC('slawomir.zych@thenets.pl');
        
        //Set the subject line
        $this->mail->Subject = 'Zgłoszenie kontaktowe UI/UX - Landing Page The Nets';
        
        $msg =' <table>
                <tr>
                    <td><img width="100px" src="cid:logo" /></td>
                    <td valign="middle" align="center" style="padding:0 0 0 30px">Witaj zgłoszenie: <b>'.$surnameName.'! </b></td>
                </tr>
            </table>
        
        <div style="background:#F7F7F7; padding:20px;">
        <br><b>Imię i nazwisko:</b> '.$surnameName.'
        <br><b>Firma: </b>'.$company.'
        <br><b>Strona www: </b>'.$www.'    
        <br><b>Opis: </b>'.$description.'   
        <br><b>Telefon:</b>'.$phone.'
        <br><b>Email: </b>'.$email.'';
        $msg.='<br><br><b>Zaakceptowane zgody:</b><br>';
        
        $msg.= 'TAK - Wyrażam zgodę na przetwarzanie moich danych osobowych w celach związanych z udzieleniem odpowiedzi na zapytanie w formularzu kontaktowym przez The Nets Sp. z o.o. z siedzibą przy ulicy Kazimierza Morawskiego 5, 30-102 Kraków.<br>';
                  
        if($agree2){
            $msg.= 'TAK - Wyrażam zgodę na przetwarzanie moich danych osobowych w celach marketingowych przez The Nets.<br>';
        }
        
        if($agree3){
            $msg.= 'TAK - Chcę otrzymywać od The Nets treści marketingowe oraz powiadomienia o publikacjach, które pomogą mi rozwijać biznes w sieci i oferty współpracy, dlatego wyrażam zgodę na wysyłanie mi drogą elektroniczną informacji marketingowych i handlowych na wskazany przeze mnie adres e-mail.<br>';                   
        }
        
        if($agree4){
            $msg.= 'TAK - Chcę mieć możliwość kontaktu telefonicznego z agencją The Nets, dlatego wyrażam zgodę na wykorzystywanie telekomunikacyjnych urządzeń końcowych, których jestem użytkownikiem, dla celów marketingu bezpośredniego.<br>';
      
        }

        $msg.= '</div>';
        $this->mail->msgHTML($msg);
  
        //send the message, check for errors
        if (!$this->mail->send()) {
            echo "Mailer Error: " . $this->mail->ErrorInfo;
        } else {
            echo "Admin message sent!";
        }
        
    }
    
}
