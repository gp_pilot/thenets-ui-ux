<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ($_POST) {
    
    include ('external-library/get-response/GetResponseAPI.class.php');
    include('scripts/config.php');
    include ('scripts/mailsender.php');   
    
    $api = new GetResponse('bdcf631d0b410fb357c2a8f2b74d9c86');
    $campaignToken = 'ap7wx';
    
    $surnameName = filter_input(INPUT_POST, 'surname_name');
    $company = filter_input(INPUT_POST, 'company');
    $description = filter_input(INPUT_POST, 'description');
    $phone = filter_input(INPUT_POST, 'phone');
    $email = filter_input(INPUT_POST, 'email');
    $www = filter_input(INPUT_POST, 'www');
    $mailValidate = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
    $agree1 = filter_input(INPUT_POST, 'agree1');
    $agree2 = filter_input(INPUT_POST, 'agree2');
    $agree3 = filter_input(INPUT_POST, 'agree3');   
    $agree4 = filter_input(INPUT_POST, 'agree4');
    $agrees= array('lp_zgoda_1'=>'TAK');   
      
    if ($surnameName && $company && $email && $www && $mailValidate && $agree1 ) {
           
        if($agree2){
            $agrees = $agrees + array('lp_zgoda_2'=>'TAK');
        }
        
        if($agree3){
            $agrees = $agrees + array('lp_zgoda_3'=>'TAK');
        }
        
        if($agree4){
            $agrees = $agrees + array('lp_zgoda_4'=>'TAK');
        }      
        
        if($www){
            $agrees = $agrees + array('www'=>$www);
        } 
                
       if($api->addContact($campaignToken,$surnameName,trim($email),'insert','',$agrees)){
           
           $em = new Mailsender();
           $em->sendToClient($email, $surnameName);
           $em->sendToUser($email,$surnameName,$www,$company,$description,$phone,$agree2,$agree3,$agree4);   
       }
               
    }else{
        $error = 1;
    }
}