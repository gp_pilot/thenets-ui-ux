$(document).ready(function(){

    $('.btn-more').click(function(){
        $(this).parent().find('.more').show();
        $(this).hide();
    });
  
 
     $("#contact-form-the-nets").validate({
         
                rules: {
                        surname_name: {
                                required: true
                        },
                        company: {
                               required: true 
                        },  
                        www: {
                               required: true 
                        }, 
                        phone: {				
                           
			},
                        email: {				
                               required: true,
                               email: true
			}
		},
                
                errorPlacement: function(error, element) {
			
			$( element )
				.closest( "form" )
					.find( "label[for='" + element.attr( "id" ) + "']" )
						.append( error );
		},
                
                submitHandler: function(form) {
                          
                    var inputs = $('#contact-form-the-nets').serialize();    
                    var error =  true;
                    $('#formAgreeModal').modal();                                 
                }        
      
       });       
       
      $('.modal-close-button').click(function(){
          $.cookie('BanerPopUp', '1', {path: '/' });
      });
      
      $('.widget-price-information-title').click(function(){
            $(".widget-price-information-body").slideToggle('fast', function(){                            
                if($('.widget-price-information-body').is(':visible')){
                   $('.widget-price-information-arrow-top').addClass('widget-price-information-arrow-bottom');
                   $('.widget-price-information-arrow-bottom').removeClass('widget-price-information-arrow-top');
                }else{
                   $('.widget-price-information-arrow-bottom').addClass('widget-price-information-arrow-top');
                   $('.widget-price-information-arrow-top').removeClass('widget-price-information-arrow-bottom');
                }
            });
       });
       
         $("#form-modal-agree").validate({
         
                
                submitHandler: function(form) {
                          
                    var error =  true;    
                    
                    for(i=1;i<=1;i++){
                        if(!$('#agree'+i).prop( "checked" )){    
                            $('.agree'+i).addClass('err'); 
                        }else{
                            $('.agree'+i).removeClass('err');   
                             error = false;
                        }      
                    }     
                                            
                    var inputsFormTheNets = $('#contact-form-the-nets').serialize();  
                    var inputsFormAgree   = $('#form-modal-agree').serialize(); 
                                        
                    if($('#agree1').prop( "checked" )){
                        
                        $.ajax({
                            method: "POST",
                            url: "send.php",
                            data: inputsFormTheNets+'&'+inputsFormAgree
                          })
                            .done(function( msg ) {
                            $('.contact-us-lead').html('<span class="thanks-contact-us"> Dziękujemy! </span>');
                        });
                        
                        $('#formAgreeModal').modal('hide');   
                    }                              
                }        
      
       });
    
}); 