/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 var flag1 = false;
 var flag2 = false;
 var flag3 = false;
  
 window.addEventListener("scroll", function (event) {
        var scroll = this.scrollY;       
        
        if($( window ).width() > 500){
            
            if (scroll > 350) {
                
                if(!flag1){
                
                    var gg1 = new JustGage({
                        id: "gg1",
                        value: '2,2',
                        min: 0,
                        max: 2,
                        symbol: ",2 mln ",     
                        donut: true,
                        gaugeWidthScale: 0.3,
                        counter: true,
                        startAnimationTime: 2000
                    });
                

                    var gg2 = new JustGage({
                       id: "gg2",
                       value: '4,5',
                       min: 0,
                       max: 4,
                       symbol: ",5 tys.",
                       donut: true,
                       gaugeWidthScale: 0.3,
                       counter: true,
                       startAnimationTime: 3000
                   });

                   var gg3 = new JustGage({
                        id: "gg3",
                        value: '39',
                        min: 0,
                        max: 39,
                        symbol: " tys.", 
                        donut: true,
                        gaugeWidthScale: 0.3,
                        counter: true,
                        startAnimationTime: 2500
                    });
                
                }
                                
                flag1 = true;
            }
        }else{
            
             if (scroll > 400) {
                
                if(!flag1){
                
                    var gg1 = new JustGage({
                        id: "gg1",
                        value: '2,2',
                        min: 0,
                        max: 2,
                        symbol: ",2 mln ",     
                        donut: true,
                        gaugeWidthScale: 0.3,
                        counter: true,
                        startAnimationTime: 2500
                    });
                }
                                
                flag1 = true;
            }
            
             if (scroll > 700) { 
                 
                 if(!flag2){
                  var gg2 = new JustGage({
                       id: "gg2",
                       value: '4,5',
                       min: 0,
                       max: 4,
                       symbol: ",5 tys.",
                       donut: true,
                       gaugeWidthScale: 0.3,
                       counter: true,
                       startAnimationTime: 2500
                   });
                   
                   flag2 = true;
               }
             }
             
             if (scroll > 900) {
                 
                 if(!flag3){ 
                 var gg3 = new JustGage({
                        id: "gg3",
                        value: '39',
                        min: 0,
                        max: 39,
                        symbol: " tys.", 
                        donut: true,
                        gaugeWidthScale: 0.3,
                        counter: true,
                        startAnimationTime: 2500
                    });
                   
                   flag3 = true;
               }
             }
        }    
    });




